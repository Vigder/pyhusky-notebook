import bindings.frontend as ph
import re


def is_valid_date(string):
    date = re.compile('\d?\d?/\d?\d?/\d\d\d\d$')
    if date.match(string) is not None:
        return True
    else:
        return False

ph.env.pyhusky_start(params={'disable_progress': True})


data = ph.env.load('/datasets/mernis/turkey.txt').map(lambda line: line.split()).cache() 

x = [year for year in range(1910, 1990+1)]

y_m = data.filter(lambda citizen: 'E' in citizen) \
        .map(lambda citizen: [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]) \
        .filter(lambda year: len(year) == 1) \
        .map(lambda year: (int(year[0][-4:]), 1)) \
        .filter(lambda year: year[0] >= 1910 and year[0] <= 1990) \
        .count_by_key() \
        .collect()

y_f = data.filter(lambda citizen: 'K' in citizen) \
        .map(lambda citizen: [citizen[index] for index in [i for i, s in enumerate(citizen) if is_valid_date(s)]]) \
        .filter(lambda year: len(year) == 1) \
        .map(lambda year: (int(year[0][-4:]), 1)) \
        .filter(lambda year: year[0] >= 1910 and year[0] <= 1990) \
        .count_by_key() \
        .collect()

data.uncache()

y_m.sort(key=lambda k: k[0])
y_f.sort(key=lambda k: k[0])

y_m_relative = [(float(i[1])/(i[1]+y_f[y_m.index(i)][1]))*100 for i in y_m]
y_f_relative = [(float(i[1])/(i[1]+y_m[y_f.index(i)][1]))*100 for i in y_f]
